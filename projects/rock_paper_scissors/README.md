# Pierre Feuille Ciseaux

## Partie graphique

- 3 boutons d'action pour choisir l'action souhaitée : pierre / feuille / ciseaux
- un compteur de tours : de 1 à 10 tours
- une ligne qui annonce le résultat de chaque tour : vainqueur / perdant
- une ligne qui affiche le total de points pour l'utilisateur / la machine

## Partie logique

- Le jeu démarre lorsque l'utilisateur fait un premier choix
- À chaque choix :
    - le compteur s'incrémente de 1 → innerText / textContent
    - faire choisir une action à la machine
    - récupérer le choix utilisateur
    - déterminer le gagnant du tour :
        - papier > pierre
        - pierre > ciseaux
        - ciseaux > papier
    - afficher le message de fin de tour
    - incrémente de 1 le total du gagnant
    - si le tour est >= 10 : affichage fin de partie

class Game {
    // ==================================================================================
    // Définition de toutes les propriétés dont on va avoir besoin dans notre logique,
    // à travers nos différentes méthodes de classe. Si on avait choisi de ne pas les
    // déclarer, il aurait fallu passer en paramètre chaque argument requis pour toutes
    // nos méthodes, ce qui alourdit la compréhension du code pour rien.
    // ==================================================================================

    /** @type {HTMLSpanElement} */
    tourCourant;

    /** @type {HTMLSpanElement} */
    totalUtilisateur;

    /** @type {HTMLSpanElement} */
    totalMachine;

    /** @type {HTMLParagraphElement} */
    message;

    /** @type {HTMLButtonElement} */
    actionPierre;

    /** @type {HTMLButtonElement} */
    actionFeuille;

    /** @type {HTMLButtonElement} */
    actionCiseaux;

    /** @type {number} */
    tourIndex;

    /** @type {number} */
    userScore;

    /** @type {number} */
    machineScore;

    // ==================================================================================
    // Première méthode appelée lors de l'instanciation de la class : `new Game()`.
    // C'est ici que l'on fait les premières vérifications pour s'assurer que notre
    // code peut effectivement fonctionner de manière normale, sinon on bloque de suite.
    // ==================================================================================
    constructor() {
        const tourCourant = document.querySelector("#tour-courant");
        const totalUtilisateur = document.querySelector("#total-utilisateur");
        const totalMachine = document.querySelector("#total-machine");
        const message = document.querySelector("#message");
        const actionPierre = document.querySelector("#action-pierre");
        const actionFeuille = document.querySelector("#action-feuille");
        const actionCiseaux = document.querySelector("#action-ciseaux");
        const restartButton = document.querySelector('#restart');

        if (
            !this.allElementsAreInDOM([
                tourCourant,
                totalUtilisateur,
                totalMachine,
                message,
                actionPierre,
                actionFeuille,
                actionCiseaux,
                restartButton,
            ])
        ) {
            return;
        }

        this.tourCourant = tourCourant;
        this.totalUtilisateur = totalUtilisateur;
        this.totalMachine = totalMachine;
        this.message = message;
        this.actionPierre = actionPierre;
        this.actionFeuille = actionFeuille;
        this.actionCiseaux = actionCiseaux;
        this.tourIndex = 1;
        this.userScore = 0;
        this.machineScore = 0;

        restartButton.addEventListener('click', () => window.location.reload());

        this.init();
    }

    // ==================================================================================
    // Lance les différents process pour que l'application puisse fonctionner.
    // Ici on branche un event listener à chacun de nos trois boutons d'action.
    // ==================================================================================
    init() {
        this.actionCiseaux.addEventListener('click', this.handleUserChoice.bind(this));
        this.actionFeuille.addEventListener('click', this.handleUserChoice.bind(this));
        this.actionPierre.addEventListener('click', this.handleUserChoice.bind(this));
    }

    // ==================================================================================
    // Méthode la plus importante de la classe. C'est elle qui réagit au choix
    // utilisateur pour ensuite lancer toute la logique du jeu.
    // ==================================================================================
    /**
     * @param {PointerEvent} event
     * @return {void}
     */
    handleUserChoice(event) {
        // Jeu déjà terminé si on est au tour 10.
        if (this.tourIndex > 9) {
            return;
        }

        /** @type {'pierre'|'feuille'|'ciseaux'} */
        let userChoice;

        if (event.target === this.actionCiseaux) {
            userChoice = 'ciseaux';
        } else if (event.target === this.actionFeuille) {
            userChoice = 'feuille';
        } else if (event.target === this.actionPierre) {
            userChoice = 'pierre';
        } else {
            console.error('unexpected');
            return;
        }

        const machineChoice = this.getRandomChoiceForMachine();
        const result = this.hasUserWon(userChoice, machineChoice);

        if (result === null) {
            this.displayMessage(`Vous avez choisi : ${userChoice}, la machine : ${machineChoice}. Vainqueur du tour : Aucun`);
        } else if (result === true) {
            this.incrementPointsFor('user');
            this.displayMessage(`Vous avez choisi : ${userChoice}, la machine : ${machineChoice}. Vainqueur du tour : Utilisateur`);
        } else {
            this.incrementPointsFor('machine');
            this.displayMessage(`Vous avez choisi : ${userChoice}, la machine : ${machineChoice}. Vainqueur du tour : Machine`);
        }

        // Gestion du dernier tour du jeu, pour afficher le vainqueur s'il y en a un.
        if (this.tourIndex === 9) {
            let winnerMessage;

            if (this.userScore === this.machineScore) {
                winnerMessage = 'Match nul';
            } else if (this.userScore > this.machineScore) {
                winnerMessage = 'Victoire GG';
            } else {
                winnerMessage = 'Défaite loser';
            }

            alert(`Fin de la partie : ${winnerMessage}`);
        }

        this.incrementTour();
    }

    // ==================================================================================
    // Récupère un choix parmi les 3 possibles de manière aléatoire.
    // ==================================================================================
    /** @return {string} */
    getRandomChoiceForMachine() {
        const availableChoices = ['pierre', 'feuille', 'ciseaux'];
        const chosenIndex = Math.floor(Math.random() * availableChoices.length);

        return availableChoices[chosenIndex];
    }

    // ==================================================================================
    // Incrémente le compteur de tours de 1.
    // ==================================================================================
    incrementTour() {
        this.tourIndex++;
        this.tourCourant.textContent = this.formatNumber(this.tourIndex);
    }

    // ==================================================================================
    // Incrémente le score de la partie pour un des 2 joueurs de +1.
    // ==================================================================================
    /**
     * @param {'user'|'machine'} type
     * @return {void}
     */
    incrementPointsFor(type) {
        if (type === 'user') {
            this.userScore++;
            this.totalUtilisateur.textContent = this.formatNumber(this.userScore);

            return;
        }

        if (type === 'machine') {
            this.machineScore++;
            this.totalMachine.textContent = this.formatNumber(this.machineScore);
        }
    }

    // ==================================================================================
    // Méthode qui permet d'afficher un message à l'utilisateur à l'endroit prévu pour.
    // ==================================================================================
    /**
     * @param {string} message
     * @return {void}
     */
    displayMessage(message) {
        this.message.textContent = message;
    }

    // ==================================================================================
    // Calcul du vainqueur du tour, en fonction des règles du jeu. Retourne un booléen
    // dans le cas où il y a bien un vainqueur, sinon retourne `null`.
    // ==================================================================================
    /**
     * @param {'pierre'|'feuille'|'ciseaux'} userChoice
     * @param {'pierre'|'feuille'|'ciseaux'} machineChoice
     * @return {boolean|null}
     */
    hasUserWon(userChoice, machineChoice) {
        if (userChoice === machineChoice) return null;

        if (userChoice === 'ciseaux' && machineChoice === 'feuille') return true;
        if (userChoice === 'ciseaux' && machineChoice === 'pierre') return false;

        if (userChoice === 'feuille' && machineChoice === 'pierre') return true;
        if (userChoice === 'feuille' && machineChoice === 'ciseaux') return false;

        if (userChoice === 'pierre' && machineChoice === 'ciseaux') return true;
        if (userChoice === 'pierre' && machineChoice === 'feuille') return false;
    }

    // ==================================================================================
    // Ajoute un 0 devant le nombre si < 10.
    // ==================================================================================
    /**
     * @param {number} number
     * @return {string}
     */
    formatNumber(number) {
        if (number < 10) {
            return `0${number}`;
        }

        return number.toString();
    }

    // ==================================================================================
    // Méthode qui permet de vérifier que l'ensemble des éléments HTML passés en
    // arguments sont bien présents dans le DOM au moment où le jeu est lancé.
    // ==================================================================================
    /**
     * @param {Array<Element|null>} elements
     * @return {boolean}
     */
    allElementsAreInDOM(elements) {
        let valid = true;

        for (let element of elements) {
            if (element === null) {
                valid = false;
            }
        }

        return valid;
    }
}

new Game();

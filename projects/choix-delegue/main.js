class App {
  // ==================================================================================
  // Définition de toutes les propriétés dont on va avoir besoin dans notre logique,
  // à travers nos différentes méthodes de classe. Si on avait choisi de ne pas les
  // déclarer, il aurait fallu passer en paramètre chaque argument requis pour toutes
  // nos méthodes, ce qui alourdit la compréhension du code pour rien.
  // ==================================================================================

  /** @type {HTMLFormElement} */
  #form;

  /** @type {HTMLButtonElement} */
  #electButton;

  /** @type {HTMLUListElement} */
  #listing;

  /** @type {Array<string>} */
  #candidats;

  // ==================================================================================
  // Première méthode appelée lors de l'instanciation de la class : `new App()`.
  // C'est ici que l'on fait les premières vérifications pour s'assurer que notre
  // code peut effectivement fonctionner de manière normale, sinon on bloque de suite.
  // ==================================================================================
  constructor() {
    const formulaire = document.querySelector('#formulaire');
    const chooseBtn = document.querySelector('#choose');
    const candidatsList = document.querySelector('#list');

    if (
        null === formulaire ||
        null === chooseBtn ||
        null === candidatsList
    ) {
      return;
    }

    this.#form = formulaire;
    this.#electButton = chooseBtn;
    this.#listing = candidatsList;
    this.#candidats = [];

    this.init();
  }

  // ==================================================================================
  // Lance les différents process pour que l'application puisse fonctionner.
  // On essaie d'abord de récupérer d'éventuelles données précédemment sauvegardées,
  // puis on branche les deux event listeners nécessaires à notre app.
  // ==================================================================================
  /** @return {void} */
  init() {
    this.refreshFromLocalStorage();

    this.#form.addEventListener('submit', this.handleSubmit.bind(this));
    this.#electButton.addEventListener('click', this.handleElect.bind(this));
  }

  // ==================================================================================
  // Récupère un nombre aléatoire entre 0 et la longueur du tableau de candidats, puis
  // retourne la valeur contenue dans le tableau à l'index choisi aléatoirement.
  // ==================================================================================
  /** @return {void} */
  handleElect() {
    const randomIndex = Math.floor(Math.random() * this.#candidats.length);
    alert(this.#candidats[randomIndex]);
  }

  // ==================================================================================
  // Récupère les éventuelles données stockées dans le localStorage. Si elles sont
  // trouvées, on les utilise comme base, on trie par ordre alphabétique puis on crée
  // la liste qui sera affichée dans le HTML.
  // ==================================================================================
  /** @return {void} */
  refreshFromLocalStorage() {
    const existingList = window.localStorage.getItem('candidats');

    if (null === existingList) {
      return;
    }

    const savedList = JSON.parse(existingList);

    if (!Array.isArray(savedList) || (Array.isArray(savedList) && savedList.length <= 0)) {
      return;
    }

    this.#candidats = savedList;
    this.sortCandidates();
    this.renderList();
  }

  // ==================================================================================
  // Action effectuée lorsque l'on soumet le formulaire. On fait quelques vérifications
  // et si tout est bon, alors on ajoute le candidat à la liste, on la trie puis on la
  // rend à nouveau dans le HTML.
  // ==================================================================================
  /** @param {SubmitEvent} event */
  handleSubmit(event) {
    event.preventDefault();

    /** @type {string|undefined}  */
    const firstname = this.#form.firstname?.value;

    if (undefined === firstname || '' === firstname) {
      return;
    }

    if (this.#candidats.includes(firstname)) {
      alert('Prénom déjà renseigné');
      return;
    }

    this.#candidats.push(firstname);
    this.sortCandidates();
    this.saveCandidates();
    this.renderList();

    this.#form.reset();
  }

  // ==================================================================================
  // Permet de créer un nouvel élément `<li>` avec le prénom du candidat. On branche
  // également l'événement au clic qui permet de supprimer le candidat de la liste.
  // ==================================================================================
  /**
   * @param {string} firstname
   * @return {HTMLLIElement}
   */
  createCandidat(firstname) {
    const listItemElement = document.createElement('li');
    listItemElement.textContent = firstname;

    const eventHandler = this.handleRemoveCandidat(listItemElement, firstname);
    listItemElement.addEventListener('click', eventHandler);

    return listItemElement;
  }

  // ==================================================================================
  // Supprime un candidat de la liste.
  // ==================================================================================
  /**
   * @param {string} firstname
   * @return {void}
   */
  removeCandidat(firstname) {
    this.#candidats = this.#candidats.filter((value) => value !== firstname);
  }

  // ==================================================================================
  // Affiche la liste des candidats.
  // On recrée notre `<ul>` à chaque nouveau rendu pour supprimer toute la liste
  // précédente et repartir sur une base propre. Sur chacun des candidats, on crée
  // le nouveau `<li>` en appelant `createCandidat` puis on l'ajoute au `<ul>` avec
  // `appendChild`.
  // ==================================================================================
  /** @return {void} */
  renderList() {
    const newList = document.createElement('ul');
    newList.id = 'list';

    for (let candidat of this.#candidats) {
      const element = this.createCandidat(candidat);
      newList.appendChild(element);
    }

    this.#listing.replaceWith(newList);
    this.#listing = newList;
  }

  // ==================================================================================
  // Suppression d'un candidat de manière visuelle et du tableau de sauvegarde, puis
  // on rend une nouvelle fois la liste.
  // ==================================================================================
  /**
   * @param {HTMLLIElement} itemElement
   * @param {string} firstname
   * @return {() => void}
   */
  handleRemoveCandidat(itemElement, firstname) {
    return () => {
      itemElement.remove();
      this.removeCandidat(firstname);
      this.sortCandidates();
      this.renderList();
      this.saveCandidates();
    }
  }

  // ==================================================================================
  // Tri des candidats par ordre alphabétique.
  // ==================================================================================
  /** @return {void} */
  sortCandidates() {
    this.#candidats.sort();
  }

  // ==================================================================================
  // Sauvegarde des candidats dans le localStorage.
  // ==================================================================================
  /** @return {void} */
  saveCandidates() {
    window.localStorage.setItem('candidats', JSON.stringify(this.#candidats));
  }
}

new App();
